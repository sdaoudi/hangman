# HANGMAN WEBSITE WITH SYMFONY 4 #

## INSTALLATION ##

Ce projet est initialis� avec du PHP73.

Lancer la commande suivante pour installer les d�pendances du projet:

	$ composer install

Pour d�marrer le serveur web et y acc�der � l'application taper la commande:

    $ php bin/console server:start 0.0.0.0:8189

Le site Hangman est accessible maitenant sur le lien [http://localhost:8189/fr](http://localhost:8189/fr)

## DOCUMENTATION ##

### Les principes [SOLID](https://blog.xebia.fr/2011/07/18/les-principes-solid#les-principes-solid) ###

Avant de commencer un nouveau d�veloppement d'une application, d'une librairie ou d'un composant: il faut toujours penser aux principes SOLID:

- **S**ingle responsibility principle
- **O**pen close principle
- **L**iskov principle
- **I**nterface segregation principle
- **D**ependency inversion principle

### Dependency Injection

- Ce composant et utilis� par l'ensemble des composants du Framework Symfony 4. Comme l'appel addDispatcher.
- L'autowiring
- R�cup�rer les services publiques avec la m�thode get(Service::class) du container.
- Le conteneur est compli� dans le cache SF (var/cache/)
- Lazy Service

### ExpressionLanguage Component

- Avec ce composant on facilement passer des conditions aux action du controlleur avec des annotations ([https://bitbucket.org/sdaoudi/hangman/src/master/src/Controller/GameController.php#lines-55)](https://bitbucket.org/sdaoudi/hangman/src/master/src/Controller/GameController.php#lines-55)

### Exceptions

- Eviter les erreurs 500 mettre des erreurs 404 ([https://bitbucket.org/sdaoudi/hangman/src/master/src/Controller/GameController.php#lines-89:93](https://bitbucket.org/sdaoudi/hangman/src/master/src/Controller/GameController.php#lines-89:93))

### Controller:

- Extend AbsctractController. Controller est d�prici�
- Override getSubscribedServices afin de garder le service en private

### PHPUnit

- @exepectedException => Tester une exception
- @ dataProvider =>  Tester plusieurs valeurs

Voir [https://bitbucket.org/sdaoudi/hangman/src/master/tests/Game/GameContextTest.php](https://bitbucket.org/sdaoudi/hangman/src/master/tests/Game/GameContextTest.php)


### Functionnal Test
 
- WebTestCase class
- BrowserKit Component
- CssSelector Component

Voir [https://bitbucket.org/sdaoudi/hangman/src/master/tests/Controller/GameControllerTest.php](https://bitbucket.org/sdaoudi/hangman/src/master/tests/Controller/GameControllerTest.php) 

### Security:

- Security Component and Bundle
- Commande security-encode-password
- Debug Config Command
- Customiser le username, password et csrf config pour une formulaire
- Live Templates dans PHPStorm
- isGranted('ROLE_ADMIN') => AccessDeniedException
- Use Annotation @IsGranted with statusCode=404
- Data Transfert Object pour la classe User
- Sp�cifier le provider dans le firewall
- Utiliser chain dans le cas de plusieurs providers

### HTTP CACHE

Le reverse proxy cache de symfony est mis en place:
[https://bitbucket.org/sdaoudi/hangman/src/master/public/index.php#lines-25](https://bitbucket.org/sdaoudi/hangman/src/master/public/index.php#lines-25)