<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GameControllerTest extends WebTestCase
{
    public function testPlayWordWithoutGame()
    {
        $client = static::createClient();

        $crawler = $client->request(Request::METHOD_POST, '/en/game/play-word', [
            'word' => 'test',
        ]);

        $this->assertSame(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    public function testPlayWord()
    {
        $client = static::createClient();

        $client->getContainer()->get('session')->set('hangman', [
            'word' => 'test',
            'attempts' => 0,
            'tried_letters' => [],
            'found_letters' => [],
        ]);

        $crawler = $client->request(Request::METHOD_GET, '/en/game');

        $this->assertCount(1, $button = $crawler->selectButton('Let me guess...'));

        $client->submit($button->form([
            'word' => 'test',
        ]));

        $this->assertSame(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());
        $this->assertSame('/en/game/won', $client->getResponse()->headers->get('location'));
    }
}
