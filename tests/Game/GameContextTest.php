<?php

namespace App\Tests\Game;

use App\Game\Game;
use App\Game\GameContext;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GameContextTest extends TestCase
{
    private $context;
    private $session;

    protected function setUp()
    {
        $this->session = $this->createMock(SessionInterface::class);
        $this->context = new GameContext($this->session);
    }

    protected function tearDown()
    {
        $this->context = null;
        $this->session = null;
    }

    public function testLoadGameReturnsFalse()
    {
        $this->assertFalse($this->context->loadGame(), 'The session should not have data.');
    }

    public function testLoadGame()
    {
        $contextData = [
            'word'          => 'test',
            'attempts'      => 0,
            'found_letters' => [],
            'tried_letters' => [],
        ];

        $this->session->expects($this->once())
            ->method('get')
            ->with('hangman')
            ->willReturn($contextData)
        ;

        $this->assertEquals(new Game('test'), $this->context->loadGame(), 'The session should have data.');
    }
}
