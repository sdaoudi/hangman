<?php

namespace App\Controller;

use App\Exception\GameRunnerException;
use App\Game\GameRunner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/game")
 */
class GameController extends Controller
{
    /**
     * @Route(name="app_game_index", methods={"GET"})
     */
    public function index(GameRunner $runner): Response
    {
        return $this->render('game/index.html.twig', [
            'game' => $runner->loadGame(),
        ]);
    }

    /**
     * @Route(
     *     "/play-letter/{letter}",
     *     name="app_game_play_letter",
     *     requirements={"letter": "[A-Z]"},
     *     methods={"GET"}
     * )
     */
    public function playLetter(GameRunner $runner, string $letter): RedirectResponse
    {
        $game = $runner->playLetter($letter);

        if ($game->isWon()) {
            return $this->redirectToRoute('app_game_won');
        }

        if ($game->isHanged()) {
            return $this->redirectToRoute('app_game_failed');
        }

        return $this->redirectToRoute('app_game_index');
    }

    /**
     * @Route(
     *     "/play-word",
     *     name="app_game_play_word",
     *     condition="request.request.getAlpha('word')",
     *     methods={"POST"}
     * )
     */
    public function playWord(GameRunner $runner, Request $request): RedirectResponse
    {
        try {
            $game = $runner->playWord($request->request->getAlpha('word'));
        } catch (GameRunnerException $e) {
            throw $this->createNotFoundException('No won game found.', $e);
        }

        if ($game->isWon()) {
            return $this->redirectToRoute('app_game_won');
        }

        return $this->redirectToRoute('app_game_failed');
    }

    /**
     * @Route("/reset", name="app_game_reset", methods={"GET"})
     */
    public function reset(GameRunner $runner): RedirectResponse
    {
        $runner->resetGame();

        return $this->redirectToRoute('app_game_index');
    }

    /**
     * @Route("/won", name="app_game_won", methods={"GET"})
     */
    public function won(GameRunner $runner): Response
    {
        try {
            $runner->resetGameOnSuccess();
        } catch (GameRunnerException $e) {
            throw $this->createNotFoundException('No won game found.', $e);
        }

        return $this->render('game/won.html.twig');
    }

    /**
     * @Route("/failed", name="app_game_failed", methods={"GET"})
     */
    public function failed(GameRunner $runner): Response
    {
        try {
            $runner->resetGameOnFailure();
        } catch (GameRunnerException $e) {
            throw $this->createNotFoundException('No failed game found.', $e);
        }

        return $this->render('game/failed.html.twig');
    }
}
