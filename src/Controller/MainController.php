<?php

namespace App\Controller;

use App\Contact\Mailer;
use App\Form\Type\ContactMessageType;
use App\Form\Type\PlayerRegistrationType;
use App\Player\PlayerManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), [
            Mailer::class,
            PlayerManager::class,
        ]);
    }

    /**
     * @Route(name="app_main_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('main/index.html.twig');
    }

    /**
     * @Route("/contact", name="app_main_contact", methods={"GET", "POST"})
     */
    public function contact(Request $request): Response
    {
        $form = $this->createForm(ContactMessageType::class, null, [
            'submit_button' => true,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get(Mailer::class)->sendMessage($form->getData());
            $this->addFlash('success', 'Thank you for sending us a message!');

            return $this->redirectToRoute('app_main_contact');
        }


        return $this->render('main/contact.html.twig', [
            'contact_form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/register", name="app_main_register", methods={"GET", "POST"})
     */
    public function register(Request $request): Response
    {
        $form = $this->createForm(PlayerRegistrationType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get(PlayerManager::class)->register($form->getData());

            $this->addFlash('success', 'Thank you for registering!');

            return $this->redirectToRoute('app_security_login');
        }


        return $this->render('main/register.html.twig', [
            'registration_form' => $form->createView(),
        ]);
    }

    /**
     * @Cache(smaxage=300)
     */
    public function sidebar()
    {
        $games = [
            'Jan 13' => 'Ultrices quisque molestie',
            'Jan 7' => 'Neque dolor eget',
            'Jan 1' => 'Sollicitudin interdum',
            'Dec 26' => 'Varius dignissim',
        ];
        $users = [
            'user 1',
            'user 2',
            'user 3',
            'user 4',
        ];

        return $this->render('main/sidebar.html.twig', [
            'games' => $games,
            'users' => $users,
        ]);
    }
}
