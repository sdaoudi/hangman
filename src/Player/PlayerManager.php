<?php

namespace App\Player;

use App\Entity\Player;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class PlayerManager
{
    private $encoder;
    private $entityManager;

    public function __construct(EncoderFactoryInterface $encoderFactory, EntityManagerInterface $entityManager)
    {
        $this->encoder = $encoderFactory->getEncoder(Player::class);
        $this->entityManager = $entityManager;
    }

    public function register(Registration $registration): void
    {
        $player = new Player(
            $registration->username,
            $this->encoder->encodePassword($registration->password, ''),
            $registration->email,
        );

        $this->entityManager->persist($player);
        $this->entityManager->flush();
    }
}
