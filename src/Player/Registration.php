<?php

namespace App\Player;

use Symfony\Component\Validator\Constraints as Assert;

class Registration
{
    /**
     * @var string|null
     *
     * @Assert\Type("string")
     * @Assert\NotBlank
     * @Assert\Length(min=3, max=80)
     */
    public $username;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     * @Assert\NotBlank
     * @Assert\Regex("/^.+$/", message="Trouve un mot de passe plus fort abruti va!")
     */
    public $password;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     * @Assert\Email
     */
    public $email;
}
