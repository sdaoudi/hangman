<?php

namespace App\Form\Type;

use App\Contact\Message;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactMessageType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Message::class,
                'submit_button' => false,
                'translation_domain' => 'form',
            ])
        ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('email', EmailType::class)
            ->add('subject', TextType::class, [
                'required' => false,
            ])
            ->add('content', TextareaType::class)
        ;

        if ($options['submit_button']) {
            $builder
                ->add('send', SubmitType::class)
            ;
        }
    }

    public function getBlockPrefix()
    {
        return 'contact';
    }
}
