<?php

namespace App\Contact;

class Mailer
{
    private $mailer;
    private $contactTo;
    private $defaultSubject;

    public function __construct(\Swift_Mailer $mailer, string $contactTo, string $contactDefaultSubject)
    {
        $this->mailer = $mailer;
        $this->contactTo = $contactTo;
        $this->defaultSubject = $contactDefaultSubject;
    }

    public function sendMessage(Message $message): bool
    {
        $mail = (new \Swift_Message($message->subject ?: $this->defaultSubject))
            ->setFrom($message->email, $message->name)
            ->setBody($message->content)
            ->setTo($this->contactTo)
        ;

        return (bool) $this->mailer->send($mail);
    }
}
